#!/bin/bash

set -e
MAKEJOBS=18


# download sources
mkdir -vp "sources"
wget -i "sources.list" -c -P "sources"

# build linux
mkdir -vp "build/linux"
tar -xf "sources/linux-4.14.92.tar.xz" -C "build/linux"
cp -vT "linux_config" "build/linux/linux-4.14.92/.config"
pushd "build"
pushd "linux/linux-4.14.92"
make -j${MAKEJOBS}
popd
cp -v "linux/linux-4.14.92/arch/x86/boot/bzImage" "bzImage"
popd

# build busybox
mkdir -vp "build/busybox"
tar -xf "sources/busybox-1.30.0.tar.bz2" -C "build/busybox"
cp -vT "busybox_config" "build/busybox/busybox-1.30.0/.config"
pushd "build/busybox/busybox-1.30.0"
make -j${MAKEJOBS}
popd

# install busybox
mkdir -vp "build/rootfs"
pushd "build/busybox/busybox-1.30.0"
make install CONFIG_PREFIX="../../rootfs"
popd

# copy etc files
cp -rv "etc" "build/rootfs"

pushd "build"

rm -vf "rootfs/linuxrc"

pushd "rootfs/etc"
echo "Build $( date '+%Y-%m-%d %H:%M' ) $( hostname )" > issue
popd

(
cat <<EOF
dir /dev 755 0 0
nod /dev/console 644 0 0 c 5 1
nod /dev/null 666 0 0 c 1 3
nod /dev/zero 666 0 0 c 1 5
nod /dev/ptmx 666 0 0 c 5 2
nod /dev/tty 666 0 0 c 5 0
nod /dev/tty0 644 0 0 c 4 0
nod /dev/tty1 644 0 0 c 4 1
nod /dev/tty2 644 0 0 c 4 2
nod /dev/tty3 644 0 0 c 4 3
nod /dev/tty4 644 0 0 c 4 4
nod /dev/random 444 0 0 c 1 8
nod /dev/urandom 444 0 0 c 1 9
slink /init bin/busybox 700 0 0
dir /proc 755 0 0
dir /sys 755 0 0
EOF

find rootfs -mindepth 1 -type d -printf "dir /%P %m 0 0\n"
find rootfs -type f -printf "file /%P %p %m 0 0\n"
find rootfs -type l -printf "slink /%P %l %m 0 0\n"
) > rootfs.list

linux/linux-4.14.92/usr/gen_init_cpio rootfs.list | gzip > rootfs.cpio.gz

popd
